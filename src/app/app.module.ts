import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


//Routes

import {APP_ROUTING} from './app.routes'

import {HeroesService} from './services/heroes.service';


import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeroeComponent } from './components/heroe/heroe.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { AboutComponent } from './components/about/about.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeroeComponent,
    HeroesComponent,
    AboutComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [
    HeroesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
